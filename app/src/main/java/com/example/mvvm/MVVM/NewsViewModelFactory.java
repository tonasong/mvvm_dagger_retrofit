package com.example.mvvm.MVVM;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.mvvm.MVVM.ViewModels.NewsViewModel;
import com.example.mvvm.Retrofit.ServiceNewApi;


public class NewsViewModelFactory implements ViewModelProvider.Factory {
    private ServiceNewApi api;

    public NewsViewModelFactory(ServiceNewApi serviceNewApi) {
        this.api = serviceNewApi;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(NewsViewModel.class)){
            return (T) new NewsViewModel(api);
        }else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
