package com.example.mvvm.MVVM.ViewModels;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm.MVVM.NewsViewModelFactory;
import com.example.mvvm.Retrofit.AllNews;
import com.example.mvvm.Retrofit.Article;
import com.example.mvvm.Retrofit.ServiceNewApi;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsViewModel extends ViewModel {

    private ServiceNewApi api;
    MutableLiveData<AllNews<Article>> dataAllArticle = new MutableLiveData<>();

    public NewsViewModel(ServiceNewApi api) {
        this.api = api;
    }

    public LiveData<AllNews<Article>> getAll(){

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://newsapi.org/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        ServiceNewApi serviceNewApi = retrofit.create(ServiceNewApi.class);

        Call<AllNews<Article>> call = api.getAllNewsInformation();
        call.enqueue(new Callback<AllNews<Article>>() {
            @Override
            public void onResponse(Call<AllNews<Article>> call, Response<AllNews<Article>> response) {
                System.out.println(response.code());
                dataAllArticle.setValue(response.body());

            }

            @Override
            public void onFailure(Call<AllNews<Article>> call, Throwable t) {
                dataAllArticle.setValue(null);
                System.out.println(t.getMessage());
            }
        });

        return dataAllArticle;
    }



}
