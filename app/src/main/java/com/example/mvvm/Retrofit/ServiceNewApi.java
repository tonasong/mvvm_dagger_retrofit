package com.example.mvvm.Retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ServiceNewApi {
    @GET("v2/everything?q=bitcoin&from=2019-11-18&sortBy=publishedAt&apiKey=4500b29c34e34bcfa5d98b04737d5f0e")
    Call<AllNews<Article>> getAllNewsInformation();
}
