package com.example.mvvm.Dagger2;

import androidx.lifecycle.ViewModelProvider;

import com.example.mvvm.MVVM.NewsViewModelFactory;
import com.example.mvvm.MVVM.ViewModels.NewsViewModel;
import com.example.mvvm.Retrofit.ServiceNewApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class MyModuls {
    @Provides
    String getString(){
        return "Hello from MyCustomModule";
    }

    @Provides
    Retrofit getRetrofit(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://newsapi.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    ServiceNewApi getServiceNewApi(){
        ServiceNewApi serviceNewApi = getRetrofit().create(ServiceNewApi.class);
        return serviceNewApi;
    }

    @Provides
    NewsViewModelFactory getFactory(){
        return new NewsViewModelFactory(getServiceNewApi());
    }


}
